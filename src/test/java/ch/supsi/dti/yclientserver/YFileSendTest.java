package ch.supsi.dti.yclientserver;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Test;

import ch.supsi.dti.yclientserver.exception.IlligealPortNumberException;

public class YFileSendTest {
	@Test
	public void testYClientServerSendFile() {
		Path path = Paths.get("./testResources/sentfiles/testFile.txt");
		try {
			Files.delete(path);
		} catch (NoSuchFileException x) {
			System.err.format("%s: no such" + " file or directory%n", path);
		} catch (DirectoryNotEmptyException x) {
			System.err.format("%s not empty%n", path);
		} catch (IOException x) {
			// File permission problems are caught here.
			System.err.println(x);
		}

		YServer server = new YServer();
		server.start(50000);

		YClient client = new YClient();
		try {
			client.start("localhost", 50000);
		} catch (IlligealPortNumberException e) {
			e.printStackTrace();
		}

		server.setSaveFilePath("./testResources/sentfiles/");

		client.sendDynamicInput("sendfile@1#1234:./testResources/testFile.txt");

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		try {
			byte[] f1 = Files.readAllBytes(Paths.get("./testResources/sentfiles/testFile.txt"));
			byte[] f2 = Files.readAllBytes(Paths.get("./testResources/testFile.txt"));
			assertTrue(Arrays.equals(f1, f2));
		} catch (IOException e) {
			fail("should not reach here : " + e.getMessage() + "\n" + e.getStackTrace());
		}
		client.stop();
		server.stop();

	}
}
