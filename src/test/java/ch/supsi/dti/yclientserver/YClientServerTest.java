package ch.supsi.dti.yclientserver;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.supsi.dti.yclientserver.exception.IlligealPortNumberException;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.messageObserver.MessageObserver;

public class YClientServerTest {
	private static volatile int commandReceivedNumber;
	private static volatile String message;
	private YServer server;
	private YClient client;

	@Before
	public void initialize() {
		commandReceivedNumber = 0;
		message = null;
		server = new YServer();
		server.start(50000);

		client = new YClient();

		try {
			client.start("localhost", 50000);
		} catch (IlligealPortNumberException e) {
			e.printStackTrace();
		}
		client.addCommandObserver(new MessageObserver() {
			@Override
			public void update(AbstractMessage commandName) {
				commandReceivedNumber++;
				message = commandName.messageToString();
				// System.out.println(commandReceivedNumber + " message: " +
				// commandName.messageToString());
			}
		});
	}

	@After
	public void finalize() {
		server.stop();
		client.stop();
	}

	@Test
	public void testClientServerWelcomeMessage() {
		System.out.println("ClientServerWelcomeMessage");
		int timeout = 0;
		while (commandReceivedNumber < 1) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (timeout++ == 100) {
				System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!TIMEOUT!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}
		}

		assertTrue("welcome messge not received! The received message is: " + message,
				"welcome to the server".equalsIgnoreCase(message));
		assertTrue("1 client should be connected instead of : " + server.getNumberOfClient(),
				1 == server.getNumberOfClient());

	}

	@Test
	public void testSingleClientServerConnection() {
		System.out.println("SingleClientServerConnection");

		for (int i = 0; i < 10; i++) {
			server.sendDynamicInput("writeOnSocketOut:Hi client 1 from server: " + i);
			client.sendDynamicInput("writeOnSocketOut@1:Hi server from client: " + i);
		}
		int tmp = 0;
		while (commandReceivedNumber < 21) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (tmp++ == 100) {
				System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!TIMEOUT!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}
		}

		assertTrue("21 messages should be observed instead of : " + commandReceivedNumber, 21 == commandReceivedNumber);
		assertTrue("1 client should be connected instead of : " + server.getNumberOfClient(),
				1 == server.getNumberOfClient());
	}
}
