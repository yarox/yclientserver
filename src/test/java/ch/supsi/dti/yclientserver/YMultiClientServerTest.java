package ch.supsi.dti.yclientserver;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.supsi.dti.yclientserver.exception.IlligealPortNumberException;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.messageObserver.MessageObserver;

public class YMultiClientServerTest {
	private static AtomicInteger commandReceivedNumber = new AtomicInteger(0);
	private static CopyOnWriteArrayList<String> messages;
	private static final int NUMBER_OF_CLIENT = 10;
	private YServer server;
	private List<YClient> clients;

	@Before
	public void initialize() {
		commandReceivedNumber.set(0);
		messages = new CopyOnWriteArrayList<String>();
		server = new YServer();
		server.start(50000);

		clients = new ArrayList<YClient>();
		for (int i = 0; i < NUMBER_OF_CLIENT; i++) {
			clients.add(new YClient());
		}
		for (YClient client : clients) {
			try {
				client.start("localhost", 50000);
			} catch (IlligealPortNumberException e) {
				e.printStackTrace();
			}
			client.addCommandObserver(new MessageObserver() {
				@Override
				public void update(AbstractMessage commandName) {
					commandReceivedNumber.incrementAndGet();
					messages.add(commandName.messageToString());
					System.out.println(commandReceivedNumber + " message: " + commandName.messageToString());
				}
			});
		}

	}

	@After
	public void finalize() {
		clients.forEach(c -> c.stop());
		server.stop();
	}

	@Test
	public void testMultiClientServerWelcomeMessage() {
		System.out.println("MultiClientServerWelcomeMessage");
		int timeout = 0;
		while (commandReceivedNumber.intValue() < 10) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (timeout++ == 100) {
				System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!TIMEOUT!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}
		}
		messages.forEach(m -> assertTrue("welcome messge not received! The received message is: " + messages,
				"welcome to the server".equalsIgnoreCase(m)));

		assertTrue(NUMBER_OF_CLIENT + " client should be connected instead of : " + server.getNumberOfClient(),
				NUMBER_OF_CLIENT == server.getNumberOfClient());

	}

	@Test
	public void testMultiClientServerConnection() {
		System.out.println("MultiClientServerConnection");

		for (int i = 0; i < 10; i++) {
			for (int j = 1; j <= NUMBER_OF_CLIENT; j++)
				server.sendDynamicInput("writeOnSocketOut@" + j + ":Hi client 1 from server: " + i);
			clients.forEach(c -> c.sendDynamicInput("writeOnSocketOut@1:Hi server from client!"));
		}
		int totalcommandreceived = 10 * NUMBER_OF_CLIENT * 2 + NUMBER_OF_CLIENT;
		int timeout = 0;
		while (commandReceivedNumber.intValue() < totalcommandreceived) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (timeout++ == 100) {
				System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!TIMEOUT!!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}
		}

		assertTrue(totalcommandreceived + " messages should be observed instead of : " + commandReceivedNumber,
				totalcommandreceived == commandReceivedNumber.intValue());
		assertTrue(NUMBER_OF_CLIENT + " client should be connected instead of : " + server.getNumberOfClient(),
				NUMBER_OF_CLIENT == server.getNumberOfClient());
	}
}
