package ch.supsi.dti.yclientserver;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.supsi.dti.yclientserver.commands.AskForUnitCommand;
import ch.supsi.dti.yclientserver.commands.CommandCreator;
import ch.supsi.dti.yclientserver.exception.IlligealPortNumberException;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.messageObserver.MessageObserver;

public class AskForUnitTest {
	private static volatile int commandReceivedNumber;
	private static volatile boolean askForUnitReceived;
	private YServer server;
	private YClient client;

	@Before
	public void initialize() {
		commandReceivedNumber = 0;
		askForUnitReceived = false;
		server = new YServer();
		server.start(50000);

		client = new YClient();

		try {
			client.start("localhost", 50000);
		} catch (IlligealPortNumberException e) {
			e.printStackTrace();
		}
		server.addCommandObserver(new MessageObserver() {
			@Override
			public void update(AbstractMessage commandName) {

				if (commandName.getCommand() instanceof AskForUnitCommand) {
					askForUnitReceived = true;
					commandReceivedNumber++;
				}
			}
		});
	}

	@After
	public void finalize() {
		server.stop();
		client.stop();
	}

	@Test
	public void testClientServerAskForUnitCommand() {
		System.out.println("ClientServerAskForUnitCommand");

		client.sendDynamicInput(CommandCreator.ASK_FOR_UNIT);
		int timeout = 0;
		while (commandReceivedNumber < 1) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (timeout++ == 100) {
				System.err.println("!!!!!!!!!!!!!!!!!!!TIMEOUT!!!!!!!!!!!!!!!!");
				break;
			}
		}
		assertTrue("askForUnitCommand not received!", askForUnitReceived);
		assertTrue("1 client should be connected instead of : " + server.getNumberOfClient(),
				1 == server.getNumberOfClient());

	}
}
