package ch.supsi.dti.yclientserver;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.supsi.dti.yclientserver.commands.WriteOnSocketOutCommand;
import ch.supsi.dti.yclientserver.exception.IlligealPortNumberException;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.messageObserver.MessageObserver;
import ch.supsi.dti.yclientserver.util.IOType;

public class YClientServerIDTest {
	private static volatile int commandReceivedNumber;
	private static volatile String message;
	private YServer server;
	private YClient client;
	private List<String> clientIds;
	private List<String> serverIds;

	@Before
	public void initialize() {
		clientIds = new CopyOnWriteArrayList<String>();
		serverIds = new CopyOnWriteArrayList<String>();
		commandReceivedNumber = 0;
		message = null;
		server = new YServer();
		server.start(50000);

		client = new YClient();

		try {
			client.start("localhost", 50000);
		} catch (IlligealPortNumberException e) {
			e.printStackTrace();
		}
		client.addCommandObserver(new MessageObserver() {
			@Override
			public void update(AbstractMessage commandName) {
				commandReceivedNumber++;

				if (commandName.getCommand() instanceof WriteOnSocketOutCommand
						&& commandName.getIotype() == IOType.SOCKET_IN)
					clientIds.add(commandName.getId());
			}
		});
		server.addCommandObserver(new MessageObserver() {
			@Override
			public void update(AbstractMessage commandName) {
				if (commandName.getCommand() instanceof WriteOnSocketOutCommand
						&& commandName.getIotype() == IOType.SOCKET_IN)
					serverIds.add(commandName.getId());
			}
		});
	}

	@After
	public void finalize() {
		server.stop();
		client.stop();
	}

	@Test
	public void testSingleClientServerIdCheck() {
		System.out.println("SingleClientServerConnection");

		for (int i = 0; i < 10; i++) {
			server.sendDynamicInput("writeOnSocketOut#11:Hi client 1 from server: " + i);
			client.sendDynamicInput("writeOnSocketOut@1#12:Hi server from client: " + i);
		}
		int timeout = 0;
		while (commandReceivedNumber < 21) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (timeout++ == 100) {
				System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!TIMEOUT!!!!!!!!!!!!!!!!!!!!!!");
				break;
			}
		}

		for (String id : clientIds) {
			assertTrue(id.equals("11"));
		}
		for (String id : serverIds) {
			assertTrue(id.equals("12"));
		}
		assertTrue(serverIds.size() == clientIds.size());
		assertTrue("21 messages should be observed instead of : " + commandReceivedNumber, 21 == commandReceivedNumber);
		assertTrue("1 client should be connected instead of : " + server.getNumberOfClient(),
				1 == server.getNumberOfClient());
	}
}
