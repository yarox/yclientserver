package ch.supsi.dti.yclientserver;

import org.junit.Test;

import ch.supsi.dti.yclientserver.exception.IlligealPortNumberException;

public class YClientTest {
	@Test(expected = IlligealPortNumberException.class)
	public void testYClientPortCheck() throws Exception{
		YClient client = new YClient();
		
		client.start("localhost", 1);
	}
	
	@Test
	public void testYClientStartStop(){
		YClient client = new YClient();
		try {
			client.start("localhost", 50000);
		} catch (IlligealPortNumberException e) {
			e.printStackTrace();
		}
		client.stop();
	}
}
