package ch.supsi.dti.yclientserver.output;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;

/**
 * 
 * @author Yari
 *
 */
public class YSocketOut implements YOutputInterface {

	private ObjectOutputStream out;

	private int socketNumber = -1;

	public YSocketOut(Socket socket) {
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			System.err.println("Fail to open socket stream from YSocketOut");
			e.printStackTrace();
		}
	}

	public YSocketOut(Socket socket, int socketNumber) {
		this(socket);
		this.socketNumber = socketNumber;
	}

	@Override
	public void sendOutput(AbstractMessage message) {
		try {
			if (message.getIotype() != IOType.SOCKET_IN) {
				out.writeObject(message);
				// out.reset();
				// out.flush();
			}
		} catch (IOException e) {
			System.err.println("Fail to send object to socket stream from YSocketOut : " + e.getMessage());
			// e.printStackTrace();
		}
	}

	@Override
	public IOType getIOType() {
		return IOType.SOCKET_OUT;
	}

	public int getSocketNumber() {
		return this.socketNumber;
	}
}
