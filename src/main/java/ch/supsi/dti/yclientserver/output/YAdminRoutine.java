package ch.supsi.dti.yclientserver.output;

import ch.supsi.dti.yclientserver.YAbstractRoutine;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;

/**
 * 
 * @author Yari
 *
 */
// TODO check if this class is still useful!! (after change con commmand style)
public class YAdminRoutine implements YOutputInterface {

	// private YAbstractRoutine routine;

	public YAdminRoutine(YAbstractRoutine clientRoutine) {
		// this.routine = clientRoutine;
	}

	@Override
	public void sendOutput(AbstractMessage message) {
		// switch (message.getCommand()) {
		// case SHUTDOWN:
		// System.out.println("SYSTEM IS SHUTTING DOWN");
		// routine.stop();
		// break;
		// default:
		// break;
		// }
	}

	@Override
	public IOType getIOType() {
		return IOType.ADMIN_ROUTINE;
	}
}
