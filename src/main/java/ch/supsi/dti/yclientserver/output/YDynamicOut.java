package ch.supsi.dti.yclientserver.output;

import ch.supsi.dti.yclientserver.util.IOType;

public interface YDynamicOut extends YOutputInterface {
	@Override
	public default IOType getIOType(){
		return IOType.DYNAMIC_OUT;
	}
}
