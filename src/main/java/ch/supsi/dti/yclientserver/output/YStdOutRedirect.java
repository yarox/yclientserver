package ch.supsi.dti.yclientserver.output;

import ch.supsi.dti.yclientserver.util.IOType;

public interface YStdOutRedirect extends YOutputInterface {
	@Override
	public default IOType getIOType(){
		return IOType.STD_OUT;
	}
}
