package ch.supsi.dti.yclientserver.output;

import ch.supsi.dti.yclientserver.commands.CommandCreator;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;

/**
 * 
 * @author Yari
 *
 */
public class YStdOut implements YOutputInterface {

	@Override
	public void sendOutput(AbstractMessage message) {
		String output = CommandCreator.PRINTED_FROM_INPUT + ":" + message.getIotype();
		if (message.getSocketNumber() != 1) {
			output += "@" + message.getSocketNumber();
		}
		if (!message.getId().equals(""))
			output += "#" + message.getId();

		output += ":" + message.messageToString();
		System.out.println(output);
	}

	@Override
	public IOType getIOType() {
		return IOType.STD_OUT;
	}

}
