package ch.supsi.dti.yclientserver.output;

import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;
/**
 * 
 * @author Yari
 *
 */
public interface YOutputInterface {
	public void sendOutput(AbstractMessage message);
	public IOType getIOType();
}
