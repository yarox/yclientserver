package ch.supsi.dti.yclientserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import ch.supsi.dti.yclientserver.commands.CommandCreator;
import ch.supsi.dti.yclientserver.input.AbstractYInput;
import ch.supsi.dti.yclientserver.input.Stoppable;
import ch.supsi.dti.yclientserver.input.YDynamicIn;
import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.input.YSocketIn;
import ch.supsi.dti.yclientserver.input.YStdIn;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.message.StandardMessage;
import ch.supsi.dti.yclientserver.output.YAdminRoutine;
import ch.supsi.dti.yclientserver.output.YOutputInterface;
import ch.supsi.dti.yclientserver.output.YSocketOut;
import ch.supsi.dti.yclientserver.output.YStdOut;
import ch.supsi.dti.yclientserver.util.IOType;

/**
 * 
 * @author Yari
 *
 */
public class YServerRoutine extends YAbstractRoutine implements Runnable {
	private ServerSocket serverSocket;

	// private Socket clientSocket;

	private Integer numberOfClient;
	private List<Socket> clients;

	private volatile boolean quitServer;

	public YServerRoutine(ServerSocket socket) {
		quitServer = false;
		this.serverSocket = socket;
		q = new ConcurrentLinkedQueue<AbstractMessage>();
		in = new CopyOnWriteArrayList<AbstractYInput>();
		out = new CopyOnWriteArrayList<YOutputInterface>();
		threads = new CopyOnWriteArrayList<Thread>();

		adminRoutine = new YAdminRoutine(this);
		out.add(adminRoutine);
		// clientSocket = null;

		dispatcher = new YInputDispatcher("ServerDistpatcher", out, q);

		numberOfClient = 0;
		clients = new CopyOnWriteArrayList<Socket>();
	}

	public YServerRoutine(ServerSocket socket, String filepath) {
		this(socket);

		dispatcher.setSaveFilePath(filepath);
	}

	@Override
	public void run() {
		Thread distpacherThread = new Thread(dispatcher);
		distpacherThread.setName("ServerDistpacher");
		distpacherThread.start();

		this.addOutput(IOType.STD_OUT);
		this.addInput(IOType.STD_IN);
		this.addInput(IOType.DYNAMIC_IN);

		while (!quitServer) {
			Socket newClient = null;
			try {
				newClient = serverSocket.accept();
				numberOfClient++;
				clients.add(newClient);
			} catch (IOException e) {
				if (e instanceof SocketException)
					System.err.println("Server socket has been closed!");
				else
					e.printStackTrace();
				break;
			}

			this.addOutput(IOType.SOCKET_OUT);
			this.addInput(IOType.SOCKET_IN);

			AbstractMessage msg = new StandardMessage(IOType.INPUT_HANDLER,
					CommandCreator.create(CommandCreator.WELCOME), "welcome to the server");
			msg.setSocketNumber(numberOfClient);
			AbstractMessage msg2 = new StandardMessage(IOType.INPUT_HANDLER,
					CommandCreator.create(CommandCreator.WRITE_ON_STDOUT), "Client accepted on the server!");
			q.add(msg);
			q.add(msg2);
		}

		try {
			distpacherThread.join();
		} catch (InterruptedException e) {
			System.err.println("distpacher thread has been interrupted!");
			e.printStackTrace();
		}
		System.out.println("Good bye!");
	}

	public void addInput(IOType type) {
		switch (type) {
		case STD_IN:
			YStdIn stdIn = new YStdIn(q);
			this.in.add(stdIn);
			Thread t = new Thread(stdIn);
			t.setName("StdIn");
			t.start();
			threads.add(t);
			break;
		case DYNAMIC_IN:
			this.in.add(new YDynamicIn(q));
			break;
		case SOCKET_IN:
			YSocketIn socketIn = new YSocketIn(q, clients.get(numberOfClient - 1), numberOfClient);
			this.in.add(socketIn);
			Thread t2 = new Thread(socketIn);
			t2.setName("SocketIn" + numberOfClient);
			t2.start();
			threads.add(t2);
			break;
		default:
			System.err.println("Unknow input type!");
			break;
		}
	}

	public void addOutput(IOType type) {
		switch (type) {
		case STD_OUT:
			YStdOut stdOut = new YStdOut();
			this.out.add(stdOut);
			break;
		case SOCKET_OUT:
			this.out.add(new YSocketOut(clients.get(numberOfClient - 1), numberOfClient));
			break;
		default:
			System.err.println("Unknow output type!");
			break;
		}
	}

	public void addOutput(YOutputInterface out) {
		this.out.add(out);
	}

	public void stop() {
		for (Socket s : clients) {
			try {
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		closeInputAndOutput();
		dispatcher.stop();

		quitServer = true;
		try {
			serverSocket.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	private void closeInputAndOutput() {
		in.forEach(input -> {
			if (input instanceof YSocketIn) {
				((Stoppable) input).stop();
				in.remove(input);
			}
		});

		out.forEach(output -> {
			if (output instanceof YSocketOut) {
				out.remove(output);
			}
		});

		threads.forEach(t -> {
			try {
				if (t.getName().equals("SocketIn")) {
					t.join();
					threads.remove(t);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

	}

	public void sendDynamicInput(String input) {
		in.forEach(in -> {
			if (in instanceof YDynamicIn)
				((YDynamicIn) in).sendInput(input);
		});
	}

	public void setSaveFilePath(String path) {
		dispatcher.setSaveFilePath(path);
	}

	public void redirectStdOut() {

	}

	public int getNumberOfClient() {
		return this.numberOfClient;
	}

}
