package ch.supsi.dti.yclientserver.commands;

import java.io.File;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.message.StandardMessage;
import ch.supsi.dti.yclientserver.util.IOType;

public class SendFileCommand implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage args) {
		if (args.getIotype() == IOType.SOCKET_IN) {
			dispatcher.sendTo(IOType.STD_OUT, args);
			StandardMessage message = new StandardMessage(IOType.SOCKET_OUT,
					CommandCreator.create(CommandCreator.START_SEND_FILE), args.messageToString(),
					args.getSocketNumber(), args.getId());
			dispatcher.sendTo(IOType.SOCKET_OUT, message);
		} else {
			if (fileExists(args.messageToString())) {
				String filename = args.messageToString();
				File file = new File(filename);
				if (file.length() < Integer.MAX_VALUE) {
					dispatcher.sendTo(IOType.SOCKET_OUT, args);
				} else {
					((StandardMessage) args).setMessage(args.messageToString() + " IS TOO LARGE!");
					dispatcher.sendTo(IOType.STD_OUT, args);
				}
			} else {
				((StandardMessage) args).setMessage(args.messageToString() + " DOES NOT EXIST!");
				dispatcher.sendTo(IOType.STD_OUT, args);
			}
		}
	}

	private boolean fileExists(String filename) {
		if (new File(filename).exists() && !new File(filename).isDirectory()) {
			return true;
		} else {
			return false;
		}
	}
}
