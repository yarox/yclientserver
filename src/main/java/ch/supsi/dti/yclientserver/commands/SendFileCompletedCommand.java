package ch.supsi.dti.yclientserver.commands;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;

public class SendFileCompletedCommand implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage args) {
		dispatcher.sendTo(IOType.STD_OUT, args);
	}

}
