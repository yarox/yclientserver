package ch.supsi.dti.yclientserver.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.message.ByteMessage;
import ch.supsi.dti.yclientserver.util.IOType;

public class StartSendFileCommand implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4772770931890577488L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage args) {
		dispatcher.sendTo(IOType.STD_OUT, args);
		String filename = args.messageToString();
		File file = new File(filename);

		Integer fileSize = (int) file.length();
		FileInputStream fileInStream;
		try {
			fileInStream = new FileInputStream(file);
			byte[] bytes = new byte[fileSize];
			fileInStream.read(bytes);
			ByteMessage byteMsg = new ByteMessage(IOType.INPUT_HANDLER,
					CommandCreator.create(CommandCreator.SENDING_FILE), filenameWithoutPath(filename), bytes);
			byteMsg.setSocketNumber(args.getSocketNumber());
			byteMsg.setId(args.getId());
			dispatcher.sendTo(IOType.SOCKET_OUT, byteMsg);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String filenameWithoutPath(String filenamewithpath) {
		return Paths.get(filenamewithpath).getFileName().toString();
	}

}
