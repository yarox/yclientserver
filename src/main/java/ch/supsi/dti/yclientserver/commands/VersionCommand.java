package ch.supsi.dti.yclientserver.commands;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.message.StandardMessage;
import ch.supsi.dti.yclientserver.util.IOType;
import ch.supsi.dti.yclientserver.util.YClientServerConstants;

public class VersionCommand implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4746535967529858916L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage args) {
		((StandardMessage)args).setMessage("VERISON: " + YClientServerConstants.VERSIONNUMBER);
		dispatcher.sendTo(IOType.STD_OUT, args);
	}

}
