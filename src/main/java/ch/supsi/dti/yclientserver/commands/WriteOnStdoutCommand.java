package ch.supsi.dti.yclientserver.commands;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;

public class WriteOnStdoutCommand implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5964992439102531298L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage args) {
		dispatcher.sendTo(IOType.STD_OUT, args);
	}

}
