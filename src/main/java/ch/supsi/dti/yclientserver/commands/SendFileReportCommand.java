package ch.supsi.dti.yclientserver.commands;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.message.StandardMessage;
import ch.supsi.dti.yclientserver.util.IOType;

public class SendFileReportCommand implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1259095269984633033L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage args) {
		dispatcher.sendTo(IOType.STD_OUT, args);
		dispatcher.sendTo(IOType.SOCKET_OUT,
				new StandardMessage(IOType.SOCKET_OUT, CommandCreator.create(CommandCreator.SEND_FILE_COMPLETED),
						"SEND FILE COMPLETED:" + args.messageToString().split(":")[1], args.getSocketNumber(),
						args.getId()));
	}
}
