package ch.supsi.dti.yclientserver.commands;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;

public class InvalidInputCommand implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2735403902788087372L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage message) {
		dispatcher.sendTo(IOType.STD_OUT, message);
	}

}
