package ch.supsi.dti.yclientserver.commands;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.message.StandardMessage;
import ch.supsi.dti.yclientserver.util.IOType;

public class SendWelcomeMessageCommand implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 547143694462064008L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage args) {
		if(args.messageToString().equals(""))
			((StandardMessage)args).setMessage("YOU ARE VERY WELCOME!!!");
		if (args.getIotype() == IOType.SOCKET_IN)
			dispatcher.sendTo(IOType.STD_OUT, args);
		else
			dispatcher.sendTo(IOType.SOCKET_OUT, args);
	}
}
