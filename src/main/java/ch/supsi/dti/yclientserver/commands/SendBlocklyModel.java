package ch.supsi.dti.yclientserver.commands;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;

public class SendBlocklyModel implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage message) {
		if (message.getIotype() != IOType.SOCKET_IN)
			dispatcher.sendTo(IOType.SOCKET_OUT, message);
	}

}
