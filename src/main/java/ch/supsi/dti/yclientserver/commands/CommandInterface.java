package ch.supsi.dti.yclientserver.commands;

import java.io.Serializable;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;

public interface CommandInterface extends Serializable {
	public void execute(YInputDispatcher dispatcher, AbstractMessage args);
}
