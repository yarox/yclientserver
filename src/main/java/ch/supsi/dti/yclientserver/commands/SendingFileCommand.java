package ch.supsi.dti.yclientserver.commands;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.message.ByteMessage;
import ch.supsi.dti.yclientserver.message.StandardMessage;
import ch.supsi.dti.yclientserver.util.IOType;

public class SendingFileCommand implements CommandInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4584417236490533544L;

	@Override
	public void execute(YInputDispatcher dispatcher, AbstractMessage args) {
		String filename = ((ByteMessage) args).getFilename();
		FileOutputStream fileOutStream;
		try {
			fileOutStream = new FileOutputStream(YInputDispatcher.sentFilePath + filename);
			fileOutStream.write(((ByteMessage) args).getMessage());
			fileOutStream.close();
			dispatcher.sendTo(IOType.SOCKET_OUT,
					new StandardMessage(IOType.SOCKET_OUT, CommandCreator.create(CommandCreator.SEND_FILE_REPORT),
							"FILE SENT SUCCESS:" + filename, args.getSocketNumber(), args.getId()));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			dispatcher.sendTo(IOType.SOCKET_OUT,
					new StandardMessage(IOType.SOCKET_OUT, CommandCreator.create(CommandCreator.SEND_FILE_REPORT),
							"FILE SENT FAILED", args.getSocketNumber(), args.getId()));
		} catch (IOException e) {
			e.printStackTrace();
			dispatcher.sendTo(IOType.SOCKET_OUT,
					new StandardMessage(IOType.SOCKET_OUT, CommandCreator.create(CommandCreator.SEND_FILE_REPORT),
							"FILE SENT FAILED", args.getSocketNumber(), args.getId()));
		}
	}

}
