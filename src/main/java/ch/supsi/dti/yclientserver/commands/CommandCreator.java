package ch.supsi.dti.yclientserver.commands;

public class CommandCreator {
	public static final String WELCOME = "sendWelcomeMessage";
	
	public static final String INVALID_COMMAND = "invalidCommand";
	
	public static final String WRITE_ON_STDOUT = "writeOnStdout";
	
	public static final String PRINTED_FROM_INPUT = "printedFromInput";
	
	public static final String WRITE_ON_SOCKETOUT = "writeOnSocketOut";
	
	public static final String SEND_FILE = "sendFile";
	
	public static final String START_SEND_FILE = "startSendFile";
	
	public static final String SENDING_FILE = "sendingFile";
	
	public static final String SEND_FILE_REPORT = "sendFileReport";
	
	public static final String SEND_FILE_COMPLETED = "sendFileCompleted";
	
	public static final String VERSION = "version";
	
	public static final String ASK_FOR_UNIT = "askForUnit";
	
	public static final String ASK_FOR_STANDARD_GATE = "askForStandardGate";
	
	public static final String SEND_BLOCKLY_MODEL = "sendBlocklyModel";
	
	public static CommandInterface create(String commandName){
		if(commandName.equalsIgnoreCase(INVALID_COMMAND)) return new InvalidInputCommand();
		if(commandName.equalsIgnoreCase(WELCOME)) return new SendWelcomeMessageCommand();
		if(commandName.equalsIgnoreCase(WRITE_ON_STDOUT)) return new WriteOnStdoutCommand();
		if(commandName.equalsIgnoreCase(PRINTED_FROM_INPUT)) return new PrintedFromInputCommand();
		if(commandName.equalsIgnoreCase(WRITE_ON_SOCKETOUT)) return new WriteOnSocketOutCommand();
		if(commandName.equalsIgnoreCase(SEND_FILE)) return new SendFileCommand();
		if(commandName.equalsIgnoreCase(START_SEND_FILE)) return new StartSendFileCommand();
		if(commandName.equalsIgnoreCase(SENDING_FILE)) return new SendingFileCommand();
		if(commandName.equalsIgnoreCase(SEND_FILE_REPORT)) return new SendFileReportCommand();
		if(commandName.equalsIgnoreCase(VERSION)) return new VersionCommand();
		if(commandName.equalsIgnoreCase(ASK_FOR_UNIT)) return new AskForUnitCommand();
		if(commandName.equalsIgnoreCase(ASK_FOR_STANDARD_GATE)) return new AskForStandardGateCommand();
		if(commandName.equalsIgnoreCase(SEND_FILE_COMPLETED)) return new SendFileCompletedCommand();
		if(commandName.equalsIgnoreCase(SEND_BLOCKLY_MODEL)) return new SendBlocklyModel();
		return null;
	}
}
