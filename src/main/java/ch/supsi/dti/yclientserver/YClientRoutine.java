package ch.supsi.dti.yclientserver;

import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import ch.supsi.dti.yclientserver.input.AbstractYInput;
import ch.supsi.dti.yclientserver.input.Stoppable;
import ch.supsi.dti.yclientserver.input.YDynamicIn;
import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.input.YSocketIn;
import ch.supsi.dti.yclientserver.input.YStdIn;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.output.YAdminRoutine;
import ch.supsi.dti.yclientserver.output.YOutputInterface;
import ch.supsi.dti.yclientserver.output.YSocketOut;
import ch.supsi.dti.yclientserver.output.YStdOut;
import ch.supsi.dti.yclientserver.util.IOType;
import ch.supsi.dti.yclientserver.util.YClientServerConstants;
/**
 * 
 * @author Yari
 *
 */
public class YClientRoutine extends YAbstractRoutine implements Runnable {
	private Socket socket;
	private volatile boolean stop;
	
	public YClientRoutine(Socket socket) {
		stop = false;
		this.socket = socket;
		q = new ConcurrentLinkedQueue<AbstractMessage>();
		in = new CopyOnWriteArrayList<AbstractYInput>();
		out = new CopyOnWriteArrayList<YOutputInterface>();
		threads = new CopyOnWriteArrayList<Thread>();
		dispatcher = new YInputDispatcher("ClientDistpatcher", out, q);
		adminRoutine = new YAdminRoutine(this);
		out.add(adminRoutine);
	}

	@Override
	public void run() {
		Thread distpacherThread = new Thread(dispatcher);
		distpacherThread.setName("ClientDistpacher");
		distpacherThread.start();
		
		System.out.println("Welcome! Your are now connected to " + socket.getInetAddress() + " at port: " + socket.getPort());
		printMenu();
		while(!stop){
			try {
				Thread.sleep(YClientServerConstants.LATENCY);
//				AbstractMessage input = q.poll();
//				if(input != null  && !out.isEmpty()){
//					//out.forEach(out -> out.putInput(input));
//					dispatcher.dispatch(input);
//				}
			} catch (InterruptedException e) {
				System.out.println("YClientRoutine has been interruped!!");
				e.printStackTrace();
			}
		}
	}
	
	public void addInput(IOType type){
		switch(type){
		case STD_IN:
			YStdIn stdIn = new YStdIn(q);
			this.in.add(stdIn);
			Thread t = new Thread(stdIn);
			t.start();
			threads.add(t);
			break;
		case DYNAMIC_IN:
			this.in.add(new YDynamicIn(q));
			break;
		case SOCKET_IN:
			YSocketIn socketIn = new YSocketIn(q, socket, 1);
			this.in.add(socketIn);
			Thread t2 = new Thread(socketIn);
			t2.start();
			threads.add(t2);
			break;
		default:
			System.err.println("Unknow input type!");
			break;
		}
	}
	
	public void addOutput(IOType type){
		switch(type){
		case STD_OUT:
			YStdOut stdOut = new YStdOut();
			this.out.add(stdOut);
			break;
		case SOCKET_OUT:
			this.out.add(new YSocketOut(socket, 1));
			break;
		default:
			System.err.println("Unknow output type!");
			break;
		}
	}
	
	public void stop(){
		System.out.println("Client routine shutting down");
			
		in.forEach(in -> {
			if(in instanceof Stoppable)
				((Stoppable)in).stop();
		});
		threads.forEach(t -> {
			try {
				t.join();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		stop = true;
	}
	
	public void sendDynamicInput(String input){
		in.forEach(in -> {
			if(in instanceof YDynamicIn)
				((YDynamicIn)in).sendInput(input);
		});
	}
	
	private void printMenu(){
		System.out.println("To write a message to the server digit : writeOnSocketOut:yourtext");
	}
	
	public void setSaveFilePath(String path) {
		dispatcher.setSaveFilePath(path);
	}
}
