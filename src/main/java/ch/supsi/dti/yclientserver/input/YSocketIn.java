package ch.supsi.dti.yclientserver.input;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Queue;

import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;
/**
 * 
 * @author Yari
 *
 */
public class YSocketIn extends AbstractYInput implements Runnable, Stoppable {

	//private BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	private Socket socket;
	private volatile boolean stop;
	private int inputNumber = -1;
	
	private ObjectInputStream in;
	
	private YSocketIn(Queue<AbstractMessage> q) {
		super(q);
	}
	
	public YSocketIn(Queue<AbstractMessage> q, Socket socket){
		super(q);
		this.socket = socket;
		stop = false;
	}
	
	public YSocketIn(Queue<AbstractMessage> q, Socket socket, int inputNumber){
		this(q, socket);
		this.inputNumber = inputNumber;
	}

	@Override
	public void run() {
		try{
			in = new ObjectInputStream(socket.getInputStream());
			while (!stop) {
				AbstractMessage message = (AbstractMessage)in.readObject();
				message.setIotype(IOType.SOCKET_IN);
				message.setSocketNumber(this.inputNumber);
				this.addInput(message);
			}
		} catch (SocketException e) {
			System.err.println("Socket has been closed!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			if(stop)
				System.err.println("YSocketIn thread has been stopped");
			else{
				if(e instanceof EOFException){
					System.err.println("EOF Exception in socket in");
				}else
					e.printStackTrace();
			}
		}finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void stop(){
		stop = true;
		try {
			if(in != null)
				in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public IOType getIOType() {
		return IOType.SOCKET_IN;
	}

}
