package ch.supsi.dti.yclientserver.input;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Queue;

import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.message.MessageUtil;
import ch.supsi.dti.yclientserver.util.IOType;
import ch.supsi.dti.yclientserver.util.YClientServerConstants;

/**
 * 
 * @author Yari
 *
 */
public class YStdIn extends AbstractYInput implements Runnable, Stoppable {
	private volatile boolean stop;

	public YStdIn(Queue<AbstractMessage> q) {
		super(q);
		stop = false;
	}

	@Override
	public void run() {
		try (BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));) {
			while (!stop) {
				// non blocking std in reading
				if (stdIn.ready()) {
					String s = stdIn.readLine();
					if (!MessageUtil.printFromInput(s)) {
						this.addInput(MessageUtil.parseMessage(s));
					}
				} else {
					Thread.sleep(YClientServerConstants.STDINLATENCY);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stop() {
		this.stop = true;
	}

	@Override
	public IOType getIOType() {
		return IOType.STD_IN;
	}
}
