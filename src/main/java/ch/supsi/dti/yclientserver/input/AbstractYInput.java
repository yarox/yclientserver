package ch.supsi.dti.yclientserver.input;

import java.util.Queue;

import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.util.IOType;
/**
 * 
 * @author Yari
 *
 */
public abstract class AbstractYInput{
	private Queue<AbstractMessage> q;
	
	public AbstractYInput(Queue<AbstractMessage> q){
		this.q = q;
	}
	
	public void addInput(AbstractMessage message){
		q.add(message);
	}
	
	public abstract IOType getIOType();
}
