package ch.supsi.dti.yclientserver.input;

import java.util.List;
import java.util.Queue;

import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.messageObserver.Subject;
import ch.supsi.dti.yclientserver.output.YOutputInterface;
import ch.supsi.dti.yclientserver.output.YSocketOut;
import ch.supsi.dti.yclientserver.util.IOType;
import ch.supsi.dti.yclientserver.util.YClientServerConstants;

/**
 * 
 * @author Yari
 *
 */
public class YInputDispatcher extends Subject implements Runnable {

	// private String name;
	private Queue<AbstractMessage> q;
	private List<YOutputInterface> outList;

	public static String sentFilePath = "";

	private volatile boolean stop;

	public YInputDispatcher(String name, List<YOutputInterface> out, Queue<AbstractMessage> q) {
		super();
		outList = out;
		// this.name = name;
		this.q = q;
		stop = false;
	}

	public YInputDispatcher(List<YOutputInterface> out, Queue<AbstractMessage> q, String filepath) {
		super();
		outList = out;
		this.q = q;
		stop = false;
	}

	private void dispatch(AbstractMessage message) {
		message.getCommand().execute(this, message);
		this.notify(message);
	}

	@Override
	public void run() {
		while (!stop) {
			try {
				Thread.sleep(YClientServerConstants.LATENCY);
				AbstractMessage input = q.poll();
				if (input != null) {
					this.dispatch(input);
				}
			} catch (InterruptedException e) {
				System.out.println("YInputDistpacher has been interruped!!");
				e.printStackTrace();
			}
		}
	}

	public void stop() {
		while (!isQueueEmpty()) {
			try {
				Thread.sleep(YClientServerConstants.LATENCY);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.stop = true;
	}

	public void setSaveFilePath(String path) {
		// TODO check wheter path ends with / or not (and make it ends WITH ! )
		YInputDispatcher.sentFilePath = path;
	}

	private boolean isQueueEmpty() {
		return q.isEmpty();
	}

	public void sendTo(IOType type, AbstractMessage message) {
		outList.forEach(outEl -> {
			if (type == IOType.SOCKET_OUT && outEl instanceof YSocketOut && message.getIotype() != IOType.SOCKET_IN) {
				if (((YSocketOut) outEl).getSocketNumber() == message.getSocketNumber())
					outEl.sendOutput(message);
			} else if (outEl.getIOType() == type)
				outEl.sendOutput(message);
		});
	}
}
