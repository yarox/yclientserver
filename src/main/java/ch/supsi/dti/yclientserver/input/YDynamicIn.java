package ch.supsi.dti.yclientserver.input;

import java.util.Queue;

import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.message.MessageUtil;
import ch.supsi.dti.yclientserver.util.IOType;

/**
 * 
 * @author Yari
 *
 */
public class YDynamicIn extends AbstractYInput {

	public YDynamicIn(Queue<AbstractMessage> q) {
		super(q);
	}

	public void sendInput(String s) {
		if (!MessageUtil.printFromInput(s)) {
			this.addInput(MessageUtil.parseMessage(s));
		}
	}

	@Override
	public IOType getIOType() {
		return IOType.DYNAMIC_IN;
	}
}
