package ch.supsi.dti.yclientserver.input;
/**
 * 
 * @author Yari
 *
 */
public interface Stoppable {
	public void stop();
}
