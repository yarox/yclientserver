package ch.supsi.dti.yclientserver;

import java.io.IOException;
import java.net.Socket;

import ch.supsi.dti.yclientserver.exception.IlligealPortNumberException;
import ch.supsi.dti.yclientserver.messageObserver.MessageObserver;
import ch.supsi.dti.yclientserver.util.IOType;
import ch.supsi.dti.yclientserver.util.YClientServerConstants;

/**
 * 
 * @author Yari
 *
 */
public class YClient {

	private YClientRoutine clientRoutine;
	private Thread clientRoutineThread;
	private Socket socket;

	public void start(String hostName, int portNumber) throws IlligealPortNumberException {

		if (portNumber < YClientServerConstants.LOWEST_PORT || portNumber > YClientServerConstants.HIGHEST_PORT) {
			throw new IlligealPortNumberException();
		}

		socket = null;

		try {
			socket = new Socket(hostName, portNumber);
		} catch (IOException e) {
			System.err.println("Connection problem be sure server is up and port number is correct !");
			System.err.println("Exception message : " + e.getMessage());
			socket = null;
			return;
		}
		clientRoutine = new YClientRoutine(socket);

		clientRoutine.addOutput(IOType.STD_OUT);
		clientRoutine.addOutput(IOType.SOCKET_OUT);
		clientRoutine.addInput(IOType.STD_IN);
		clientRoutine.addInput(IOType.SOCKET_IN);
		clientRoutine.addInput(IOType.DYNAMIC_IN);

		clientRoutineThread = new Thread(clientRoutine);
		clientRoutineThread.start();
	}

	public void stop() {
		if (clientRoutine != null) {
			clientRoutine.stop();
			try {
				clientRoutineThread.join();
				socket.close();
			} catch (InterruptedException e) {
				System.err.println("error on stop clientRoutineThread");
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				socket = null;
			}
		}
		System.out.println("Client stopped!");
	}

	public void sendDynamicInput(String input) {
		if (socket != null)
			clientRoutine.sendDynamicInput(input);
		else
			System.out.println("client not connected!!!!");
	}

	public void addCommandObserver(MessageObserver ob) {
		this.clientRoutine.addCommandObserver(ob);
	}

	public boolean isConnected() {
		return socket != null;
	}

	public void setSaveFilePath(String path) {
		if (clientRoutine != null)
			clientRoutine.setSaveFilePath(path);
	}
}
