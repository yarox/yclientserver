package ch.supsi.dti.yclientserver.util;
/**
 * 
 * @author Yari
 *
 */
public enum IOType {
	STD_IN("stdIn"),
	DYNAMIC_IN("dynamicIn"),
	SOCKET_IN("socketIn"),
	STD_OUT("stdOut"),
	DYNAMIC_OUT("dynamicOut"),
	SOCKET_OUT("socketOut"),
	INPUT_HANDLER("inputHandler"),
	ADMIN_ROUTINE("adminroutine");
	
	private final String description;
	
	IOType(String d){
		this.description = d;
	}
	
	public String getDesc() { return description; }
}