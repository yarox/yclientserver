package ch.supsi.dti.yclientserver.util;

/**
 * 
 * @author Yari
 *
 */
public class YClientServerConstants {
	public static final String WELCOME = "WelcomeMsg";

	public static final int FTP_PACKETSIZE = 4096; // with higher size it may
													// not working

	public static final String ADMIN_PREFIX = "YADMIN";

	public static final String SHUTDOWN = ADMIN_PREFIX + "shut down";

	// DO NOT SET LATENCY TO 0
	public static final int LATENCY = 10; // latency of input/queue checking in
											// millisec (this influenses the
											// transimmison speed)

	public static final int STDINLATENCY = 50;

	public static final int LOWEST_PORT = 49152;

	public static final int HIGHEST_PORT = 65535;

	public static final String VERSIONNUMBER = "1.0.08";
}
