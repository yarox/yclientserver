package ch.supsi.dti.yclientserver;

import java.util.List;
import java.util.Queue;

import ch.supsi.dti.yclientserver.input.AbstractYInput;
import ch.supsi.dti.yclientserver.input.YInputDispatcher;
import ch.supsi.dti.yclientserver.message.AbstractMessage;
import ch.supsi.dti.yclientserver.messageObserver.MessageObserver;
import ch.supsi.dti.yclientserver.output.YAdminRoutine;
import ch.supsi.dti.yclientserver.output.YOutputInterface;
/**
 * 
 * @author Yari
 *
 */
public abstract class YAbstractRoutine{
	protected volatile boolean stop;
	
	protected List<AbstractYInput> in;
	protected List<YOutputInterface> out;
	protected List<Thread> threads;
	
	protected YAdminRoutine adminRoutine;
	
	protected YInputDispatcher dispatcher;
	
	protected Queue<AbstractMessage> q;

	public abstract void stop();
	
	public void addCommandObserver(MessageObserver ob){
		this.dispatcher.attach(ob);
	}
}
