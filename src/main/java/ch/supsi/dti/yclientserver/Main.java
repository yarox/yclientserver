package ch.supsi.dti.yclientserver;

import ch.supsi.dti.yclientserver.exception.IlligealPortNumberException;
import ch.supsi.dti.yclientserver.util.YClientServerConstants;
/**
 * 
 * @author Yari
 *
 */
public class Main {

	public static void main(String[] args) {
		if(args.length == 2){
			if(args[0].equalsIgnoreCase("server")){
				int portNumber = Integer.parseInt(args[1]);
				startServer(portNumber);
			}else if(args[0].equalsIgnoreCase("client")){
				int portNumber = Integer.parseInt(args[1]);
				startClient("localhost", portNumber);
			}else
				printUsage();
		}else if(args.length == 3){
			if(args[0].equalsIgnoreCase("client")){
				int portNumber = Integer.parseInt(args[1]);
				startClient(args[2], portNumber);
			}else
				printUsage();
		}else{
			printUsage();
		}
	}
	
	public static void printUsage(){
		System.out.println("Insert as arg0 \"Server\" or \"Client\"");
		System.out.println("Insert as arg1 the port numbner from " + YClientServerConstants.LOWEST_PORT 
				+ " to " + YClientServerConstants.HIGHEST_PORT);
		System.out.println("If Client as arg2 insert the hostname (default is localhost)");
	}
	
	public static void startServer(int portNumber){
		YServer server = new YServer();
		server.start(portNumber);
	}
	
	public static void startClient(String host, int portNumber){
		try {
			YClient client = new YClient();
			client.start(host, portNumber);
		} catch (IlligealPortNumberException e) {
			System.err.println("error starting client from main method : Illegal port number");
			e.printStackTrace();
		} 
	}

}
