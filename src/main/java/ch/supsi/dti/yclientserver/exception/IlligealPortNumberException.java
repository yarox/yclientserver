package ch.supsi.dti.yclientserver.exception;
/**
 * 
 * @author Yari
 *
 */
public class IlligealPortNumberException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public IlligealPortNumberException(){
		super();
	}
}
