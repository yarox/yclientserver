package ch.supsi.dti.yclientserver;

import java.io.IOException;
import java.net.ServerSocket;

import ch.supsi.dti.yclientserver.messageObserver.MessageObserver;
import ch.supsi.dti.yclientserver.output.YOutputInterface;
/**
 * 
 * @author Yari
 *
 */
public class YServer {
	private YServerRoutine serverRoutine=null;
	private Thread serverRoutineThread;
	
	public void start(int portNumber) {
		ServerSocket serverSocket = null;
		
		try {
			serverSocket = new ServerSocket(portNumber);
		} catch (IOException e) {
			System.out.println("Impossible to create ServerSocket...");
			System.out.println(e.getMessage());
			return;
		}

		serverRoutine = new YServerRoutine(serverSocket);

		serverRoutineThread = new Thread(serverRoutine);
		serverRoutineThread.start();

		System.out.println("Server Started on port: " + portNumber);
		
	}
	
	public void setSaveFilePath(String path){
		serverRoutine.setSaveFilePath(path);
	}
	
	public void stop(){
		serverRoutine.stop();
		try {
			serverRoutineThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void sendDynamicInput(String input) {
		this.serverRoutine.sendDynamicInput(input);
	}
	
	public void addOutput(YOutputInterface out){
		this.serverRoutine.addOutput(out);
	}
	
	public void addCommandObserver(MessageObserver ob){
		this.serverRoutine.addCommandObserver(ob);
	}
	
	public int getNumberOfClient(){
		return serverRoutine.getNumberOfClient();
	}
}
