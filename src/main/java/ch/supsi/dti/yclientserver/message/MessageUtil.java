package ch.supsi.dti.yclientserver.message;

import ch.supsi.dti.yclientserver.commands.CommandCreator;
import ch.supsi.dti.yclientserver.commands.CommandInterface;
import ch.supsi.dti.yclientserver.commands.PrintedFromInputCommand;
import ch.supsi.dti.yclientserver.util.IOType;

public class MessageUtil {
	public static AbstractMessage parseMessage(String s) {
		String args;
		AbstractMessage msg;
		String commandName;
		int clientNumber = 1;
		String id;

		commandName = getCommandName(s);
		clientNumber = getClientNumber(s);
		id = getId(s);

		CommandInterface command = CommandCreator.create(commandName);

		if (command == null) {
			return new StandardMessage(IOType.STD_IN, CommandCreator.create(CommandCreator.INVALID_COMMAND),
					"UNKNOWN COMMANDTYPE OR COMMAND!");
		}

		if (s.split(":").length > 1)
			args = s.substring(s.split(":")[0].length() + 1, s.length());
		else
			args = "";

		msg = new StandardMessage(IOType.STD_IN, command, args, clientNumber, id);

		return msg;
	}

	private static String getCommandName(String input) {
		String commandName = "";
		String commandString = "";
		if (input.split(":").length != 0) {
			commandString = (input.split(":")[0]).toUpperCase();
			if (commandString.split("@").length > 1) {
				commandName = commandString.split("@")[0];
			} else {
				if (commandString.split("#").length > 1) {
					commandName = commandString.split("#")[0];
				} else
					commandName = commandString;
			}
		} else {
			commandName = input;
			if (commandString.split("@").length > 1) {
				commandName = commandString.split("@")[0];
			} else {
				if (commandString.split("#").length > 1) {
					commandName = commandString.split("#")[0];
				} else
					commandName = commandString;
			}
		}
		return commandName;
	}

	private static int getClientNumber(String input) {
		String commandString = "";
		int clientNumber = 1;
		if (input.split(":").length != 0) {
			commandString = (input.split(":")[0]).toUpperCase();
			if (commandString.split("@").length > 1) {
				commandString = commandString.split("@")[1];
				if (commandString.split("#").length > 1) {
					commandString = commandString.split("#")[0];
					clientNumber = Integer.parseInt(commandString);
				} else
					clientNumber = Integer.parseInt(commandString);
			}
		} else {
			commandString = input;
			if (commandString.split("@").length > 1) {
				commandString = commandString.split("@")[1];
				if (commandString.split("#").length > 1) {
					commandString = commandString.split("#")[0];
					clientNumber = Integer.parseInt(commandString);
				} else
					clientNumber = Integer.parseInt(commandString);
			}
		}
		return clientNumber;
	}

	private static String getId(String input) {
		String commandString = "";
		String id = "";
		if (input.split(":").length != 0) {
			commandString = (input.split(":")[0]).toUpperCase();
			if (commandString.split("#").length > 1) {
				id = commandString.split("#")[1];
			}
		} else {
			commandString = input;
			if (commandString.split("#").length > 1) {
				id = commandString.split("#")[1];
			}
		}
		return id;
	}

	public static boolean printFromInput(String s) {
		CommandInterface commandType = CommandCreator.create(getCommandName(s));
		return commandType instanceof PrintedFromInputCommand;
	}
}
