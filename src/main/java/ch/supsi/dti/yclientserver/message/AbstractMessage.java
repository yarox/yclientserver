package ch.supsi.dti.yclientserver.message;

import java.io.Serializable;

import ch.supsi.dti.yclientserver.commands.CommandInterface;
import ch.supsi.dti.yclientserver.util.IOType;

/**
 * 
 * @author Yari
 *
 */
public abstract class AbstractMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CommandInterface command;
	private IOType iotype;
	private int socketNumber = 1;
	private String id = "";

	public AbstractMessage(IOType iotype, CommandInterface command) {
		this.command = command;
		this.iotype = iotype;
	}

	public AbstractMessage(IOType iotype, CommandInterface command, int socketNumber) {
		this(iotype, command);
		this.socketNumber = socketNumber;
	}

	public AbstractMessage(IOType iotype, CommandInterface command, String id) {
		this(iotype, command);
		this.id = id;
	}

	public AbstractMessage(IOType iotype, CommandInterface command, int socketNumber, String id) {
		this(iotype, command, socketNumber);
		this.id = id;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getSocketNumber() {
		return socketNumber;
	}

	public void setSocketNumber(int socketNumber) {
		this.socketNumber = socketNumber;
	}

	public abstract String messageToString();

	public CommandInterface getCommand() {
		return this.command;
	}

	public IOType getIotype() {
		return this.iotype;
	}

	public void setIotype(IOType io) {
		this.iotype = io;
	}
}
