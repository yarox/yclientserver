package ch.supsi.dti.yclientserver.message;

import ch.supsi.dti.yclientserver.commands.CommandInterface;
import ch.supsi.dti.yclientserver.util.IOType;
/**
 * 
 * @author Yari
 *
 */
public class ByteMessage extends AbstractMessage{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private byte[] message;
	private int number;
	private String filename;
	
	public ByteMessage(IOType iotype, CommandInterface command, byte[] message) {
		super(iotype, command);
		this.message = message;
		this.number = -1;
		this.filename = "";
	}
	public ByteMessage(IOType iotype, CommandInterface command, String filename, byte[] message) {
		super(iotype, command);
		this.message = message;
		this.number = -1;
		this.filename = filename;
	}
	public ByteMessage(IOType iotype, CommandInterface command, byte[] message, int number) {
		super(iotype, command);
		this.message = message;
		this.number = number;
		this.filename = "";
	}

	@Override
	public String messageToString() {
		return "ByteMessage.messageToString() Not implemented yet!";
	}
	
	public byte[] getMessage(){
		return this.message;
	}
	
	public int getNumber(){
		return number;
	}
	
	public String getFilename(){
		return this.filename;
	}
}
