package ch.supsi.dti.yclientserver.message;

import ch.supsi.dti.yclientserver.commands.CommandInterface;
import ch.supsi.dti.yclientserver.util.IOType;

/**
 * 
 * @author Yari
 *
 */
public class StandardMessage extends AbstractMessage {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public StandardMessage(IOType iotype, CommandInterface command) {
		super(iotype, command);
		this.message = "";
	}

	public StandardMessage(IOType iotype, CommandInterface command, String message) {
		this(iotype, command);
		this.message = message;
	}

	public StandardMessage(IOType iotype, CommandInterface command, int socketNumber) {
		this(iotype, command);
		this.setSocketNumber(socketNumber);
	}

	public StandardMessage(IOType iotype, CommandInterface command, String message, int socketNumber) {
		this(iotype, command, message);
		this.setSocketNumber(socketNumber);
	}

	public StandardMessage(IOType iotype, CommandInterface command, String message, int socketNumber, String id) {
		super(iotype, command, socketNumber, id);
		this.message = message;
	}

	@Override
	public String messageToString() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
