package ch.supsi.dti.yclientserver.messageObserver;

import java.util.ArrayList;
import java.util.List;

import ch.supsi.dti.yclientserver.message.AbstractMessage;

public abstract class Subject {
	private List<MessageObserver> observers;
	
	public Subject(){
		this.observers = new ArrayList<MessageObserver>();
	}
	
	public void attach(MessageObserver observer){
		this.observers.add(observer);
	}
	
	public void detachAll(){
		this.observers.clear();
	}
	
	public void notify(AbstractMessage msg){
		observers.forEach(ob -> ob.update(msg));
	}
}
