package ch.supsi.dti.yclientserver.messageObserver;

import ch.supsi.dti.yclientserver.message.AbstractMessage;

public abstract class MessageObserver {
	
	public abstract void update(AbstractMessage commandName);
}
